import Camera, { CameraPosition, CameraParameters } from "./modules/camera";
import { Scene } from "three";
import Renderer from "./modules/renderer";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import Light from "./modules/light";

const setup = () => {
  //* renderer div element
  const renderDiv = document.getElementById("render");

  //* some initial configuration values
  let width = window.innerWidth;
  let height = window.innerHeight;
  const cameraPosition: CameraPosition = {
    x: 0,
    y: 600,
    z: 600,
  };
  const cameraParameters: CameraParameters = {
    fov: 90,
    near: 0.1,
    far: 10000,
  };

  //* scene
  const scene = new Scene();

  //* renderer
  const renderer = new Renderer(width, height, scene);
  renderer.setClearColor(0x303036);
  renderDiv.appendChild(renderer.getDomElement());

  //* camera with orbit controls
  const camera = new Camera(cameraPosition, cameraParameters, width / height);
  const orbitControl = new OrbitControls(camera.getCamera(), renderer.getDomElement());
  orbitControl.enablePan = false;
  orbitControl.enableZoom = true;
  orbitControl.maxPolarAngle = Math.PI / 3;
  orbitControl.addEventListener("change", () => {
    renderer.renderOnce(camera);
  });
  camera.lookAt(scene.position);
  scene.add(camera.getCamera());

  //* light
  const light = new Light(0xffffff, 1, { x: 0, y: 500, z: 0 });
  // scene.add(light.getLight());

  //* window resize handler
  window.onresize = () => {
    width = window.innerWidth;
    height = window.innerHeight;
    renderer.setSize(width, height);
    camera.setAspect(width / height);
    renderer.renderOnce(camera);
  };

  return { scene, renderer, camera, light };
};

export default setup;
