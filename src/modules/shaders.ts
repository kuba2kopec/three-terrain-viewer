import { Color } from "three";

const vertexShader = () => `
    varying float vAmount;

    void main()
    {
      vAmount = (position.y / 400.0) + 0.05;

      gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }
  `;

const fragmentShader = () => `
    varying float vAmount;

    void main() 
    {
      gl_FragColor = vec4(vAmount, vAmount, vAmount, 0.5);
    }  
  `;

const uniforms = {
  colorA: { type: "vec3", value: new Color(0x1111ee) },
  colorB: { type: "vec3", value: new Color(0xee1111) },
};

export { uniforms, vertexShader, fragmentShader };
