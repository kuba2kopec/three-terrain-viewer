import { Vector2, Vector3 } from "three";
import Camera from "./camera";

interface KeysPressed {
  up: boolean;
  down: boolean;
  left: boolean;
  right: boolean;
}

interface MovementData {
  isMoving: boolean;
  deltaX: number;
  deltaZ: number;
}

class Movement {
  camera: Camera;
  movementMultiplier: number;
  keysPressed: KeysPressed;

  constructor(camera: Camera, movementMultiplier: number = 3) {
    this.camera = camera;
    this.keysPressed = {
      up: false,
      down: false,
      left: false,
      right: false,
    };

    this.movementMultiplier = movementMultiplier;

    window.addEventListener("keydown", (e) => {
      switch (e.key) {
        case "w":
        case "ArrowUp":
          this.keysPressed.up = true;
          break;
        case "s":
        case "ArrowDown":
          this.keysPressed.down = true;
          break;
        case "a":
        case "ArrowLeft":
          this.keysPressed.left = true;
          break;
        case "d":
        case "ArrowRight":
          this.keysPressed.right = true;
          break;
      }
    });

    window.addEventListener("keyup", (e) => {
      switch (e.key) {
        case "w":
        case "ArrowUp":
          this.keysPressed.up = false;
          break;
        case "s":
        case "ArrowDown":
          this.keysPressed.down = false;
          break;
        case "a":
        case "ArrowLeft":
          this.keysPressed.left = false;
          break;
        case "d":
        case "ArrowRight":
          this.keysPressed.right = false;
          break;
      }
    });
  }

  calculateMovement = (): MovementData => {
    const forwardMovement =
      (!this.keysPressed.up && !this.keysPressed.down) ||
      (this.keysPressed.up && this.keysPressed.down)
        ? 0
        : this.keysPressed.up
        ? 1
        : -1;
    const sidewaysMovement =
      (!this.keysPressed.right && !this.keysPressed.left) ||
      (this.keysPressed.right && this.keysPressed.left)
        ? 0
        : this.keysPressed.right
        ? 1
        : -1;

    if (forwardMovement === 0 && sidewaysMovement === 0)
      return { isMoving: false, deltaX: 0, deltaZ: 0 };

    let cameraVector: Vector3 = this.camera.getWorldDirection();

    const zeroVector = new Vector2(0, 0);

    const directionVector = new Vector2(cameraVector.x, cameraVector.z)
      .normalize()
      .rotateAround(zeroVector, new Vector2(forwardMovement, sidewaysMovement).angle())
      .multiplyScalar(this.movementMultiplier);

    return {
      isMoving: true,
      deltaX: Math.round(directionVector.x),
      deltaZ: Math.round(directionVector.y),
    };
  };
}

export default Movement;
