import { Color, PointLight } from "three";

class Light {
  light: PointLight;

  constructor(
    color: string | number | Color,
    intensity: number,
    position: { x: number; y: number; z: number },
    distance: number = 0,
  ) {
    this.light = new PointLight(color, intensity, distance);
    this.light.position.set(position.x, position.y, position.z);
  }

  getLight = () => this.light;
}

export default Light;
