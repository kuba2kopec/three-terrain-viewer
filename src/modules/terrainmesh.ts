import { PlaneGeometry, MeshBasicMaterial, Mesh, DoubleSide } from "three";
import terrain from "../heightmap1d";

class TerrainMesh {
  mesh: Mesh;

  constructor() {
    const geometry = new PlaneGeometry(10800, 10800, 1080, 1080);
    const material = new MeshBasicMaterial({
      color: 0xe4e4e4,
      side: DoubleSide,
      wireframe: true,
    });
    for (let i = 0; i < geometry.vertices.length; i++) {
      geometry.vertices[i].z = terrain[i];
    }
    this.mesh = new Mesh(geometry, material);
    this.mesh.rotation.x = -Math.PI / 2;
  }

  getTerrainMesh() {
    return this.mesh;
  }
}

export default TerrainMesh;
