import { PerspectiveCamera, Vector3 } from "three";

export interface CameraPosition {
  x: number;
  y: number;
  z: number;
}

export interface CameraParameters {
  fov: number;
  near: number;
  far: number;
}

class Camera {
  position: CameraPosition;
  parameters: CameraParameters;
  camera: PerspectiveCamera;
  aspect: number;

  constructor(position: CameraPosition, parameters: CameraParameters, aspect: number) {
    this.position = position;
    this.parameters = parameters;
    this.aspect = aspect;

    this.camera = new PerspectiveCamera(
      this.parameters.fov,
      this.aspect,
      this.parameters.near,
      this.parameters.far,
    );

    this.camera.position.x = this.position.x;
    this.camera.position.y = this.position.y;
    this.camera.position.z = this.position.z;
  }

  getCamera = () => this.camera;

  setAspect = (aspect: number) => {
    this.aspect = aspect;
    this.camera.aspect = aspect;
    this.camera.updateProjectionMatrix();
  };

  setFOV = (fov: number) => {
    this.parameters.fov = fov;
    this.camera.fov = fov;
    this.camera.updateProjectionMatrix();
  };

  setNear = (near: number) => {
    this.parameters.near = near;
    this.camera.near = near;
    this.camera.updateProjectionMatrix();
  };

  setFar = (far: number) => {
    this.parameters.far = far;
    this.camera.far = far;
    this.camera.updateProjectionMatrix();
  };

  setPosition = (position: CameraPosition) => {
    this.position = position;
    this.camera.position.set(position.x, position.y, position.z);
  };

  lookAt = (vector: number | Vector3, y?: number, z?: number) => {
    this.camera.lookAt(vector, y, z);
  };

  getWorldDirection = () => {
    let target: Vector3;
    return this.camera.getWorldDirection(target) || target;
  };
}

export default Camera;
