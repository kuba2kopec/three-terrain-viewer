import { PlaneGeometry, MeshBasicMaterial, Mesh, DoubleSide } from "three";

class Grid {
  plane: Mesh;

  constructor() {
    const geometry = new PlaneGeometry(500, 500, 20, 20);
    const material = new MeshBasicMaterial({
      color: 0xe4e4e4,
      side: DoubleSide,
      wireframe: true,
    });
    this.plane = new Mesh(geometry, material);
    this.plane.rotation.x = Math.PI / 2;
  }

  getGrid() {
    return this.plane;
  }
}

export default Grid;
