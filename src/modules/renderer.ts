import { WebGLRenderer, Scene } from "three";
import Camera from "./camera";

class Renderer {
  renderer: WebGLRenderer;
  width: number;
  height: number;
  scene: Scene;

  constructor(width: number, height: number, scene: Scene, antialias: boolean = true) {
    this.width = width;
    this.height = height;
    this.scene = scene;

    this.renderer = new WebGLRenderer({
      antialias,
    });

    this.renderer.setSize(this.width, this.height); //rozmiary renderowanego okna
  }

  getDomElement = () => this.renderer.domElement;

  renderOnce = (camera: Camera) => {
    this.renderer.render(this.scene, camera.getCamera());
  };

  setSize = (width: number, height: number) => {
    this.width = width;
    this.height = height;
    this.renderer.setSize(this.width, this.height);
  };

  setClearColor = (color: number) => {
    this.renderer.setClearColor(color);
  };
}

export default Renderer;
