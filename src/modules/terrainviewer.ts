import { Mesh, Geometry, Face3, Vector3 } from "three";
import terrain from "../heightmap2d16b";
import Material, { Materials } from "./material";

interface Position {
  x: number;
  z: number;
}

class TerrainViewer {
  mesh: Mesh;
  cellSize: number;
  gridSize: number;
  planeSize: number;
  heightMultiplier: number;
  terrain: number[][];
  geometry: Geometry;
  material: Material;
  position: Position;

  constructor(cellSize: number = 3, gridSize: number = 1080, heightMultiplier: number = 1) {
    this.cellSize = cellSize;
    this.gridSize = gridSize;
    this.planeSize = cellSize * gridSize;
    this.heightMultiplier = heightMultiplier;

    this.terrain = terrain;

    this.position = {
      x: (this.terrain.length - 1) / 2,
      z: (this.terrain.length - 1) / 2,
    };

    this.material = new Material(Materials.Shader);
    this.geometry = new Geometry();
    this.createVertices();
    this.createFaces();
    this.updateGeometry();

    this.mesh = new Mesh(this.geometry, this.material.getMaterial());
  }

  createVertices = () => {
    for (let z = 0; z <= this.gridSize; z++) {
      for (let x = 0; x <= this.gridSize; x++) {
        const startOffset = (this.gridSize * this.cellSize) / -2;
        const vertice = new Vector3(
          startOffset + x * this.cellSize,
          0,
          startOffset + z * this.cellSize,
        );
        this.geometry.vertices.push(vertice);
        this.geometry.verticesNeedUpdate = true;
      }
    }
  };

  createFaces = () => {
    for (let z = 0; z < this.gridSize; z++) {
      for (let x = 0; x < this.gridSize; x++) {
        const face13 = z * (this.gridSize + 1) + x;
        const face12 = z * (this.gridSize + 1) + x + 1;
        const face11 = (z + 1) * (this.gridSize + 1) + x;

        const face23 = z * (this.gridSize + 1) + x + 1;
        const face22 = (z + 1) * (this.gridSize + 1) + x + 1;
        const face21 = (z + 1) * (this.gridSize + 1) + x;

        this.geometry.faces.push(new Face3(face11, face12, face13, new Vector3(0, 1, 0)));
        this.geometry.faces.push(new Face3(face21, face22, face23, new Vector3(0, 1, 0)));
      }
    }
    this.geometry.computeFaceNormals();
    this.geometry.computeBoundingBox();
    this.geometry.computeBoundingSphere();
  };

  updateGeometry = () => {
    for (let z = 0; z <= this.gridSize; z++) {
      for (let x = 0; x <= this.gridSize; x++) {
        const zOffset = this.position.z - this.gridSize / 2;
        const xOffset = this.position.x - this.gridSize / 2;

        const verticeIndex = z * (this.gridSize + 1) + x;

        this.geometry.vertices[verticeIndex].y =
          this.terrain[z + zOffset][x + xOffset] * (246 / 62996) * this.heightMultiplier;
      }
    }
    this.geometry.verticesNeedUpdate = true;
    this.geometry.computeFaceNormals();
  };

  moveTo = (position: Position) => {
    const newPosition: Position = { ...position };
    if (newPosition.x < this.gridSize / 2) {
      newPosition.x = this.gridSize / 2;
    }
    if (newPosition.z < this.gridSize / 2) {
      newPosition.z = this.gridSize / 2;
    }
    if (newPosition.x > this.terrain.length - 1 - this.gridSize / 2) {
      newPosition.x = this.terrain.length - 1 - this.gridSize / 2;
    }
    if (newPosition.z > this.terrain.length - 1 - this.gridSize / 2) {
      newPosition.z = this.terrain.length - 1 - this.gridSize / 2;
    }
    this.position = newPosition;
    this.updateGeometry();
  };

  getViewer() {
    return this.mesh;
  }
}

export default TerrainViewer;

//! <3
