import {
  DoubleSide,
  MeshBasicMaterial,
  MeshNormalMaterial,
  MeshPhongMaterial,
  ShaderMaterial,
} from "three";
import { fragmentShader, uniforms, vertexShader } from "./shaders";

export enum Materials {
  Normal = "Normal",
  Shader = "Shader",
  Wireframe = "Wireframe",
  Phong = "Phong",
}

type MaterialType =
  | {
      type: Materials.Normal;
      material: MeshNormalMaterial;
    }
  | {
      type: Materials.Shader;
      material: ShaderMaterial;
    }
  | {
      type: Materials.Wireframe;
      material: MeshBasicMaterial;
    }
  | {
      type: Materials.Phong;
      material: MeshPhongMaterial;
    };

class Material {
  material: MaterialType;
  shaderMaterial: ShaderMaterial;

  constructor(materialType: Materials) {
    switch (materialType) {
      case Materials.Normal:
        this.material = {
          type: Materials.Normal,
          material: new MeshNormalMaterial(),
        };
        break;
      case Materials.Shader:
        this.material = {
          type: Materials.Shader,
          material: new ShaderMaterial({
            uniforms: uniforms,
            fragmentShader: fragmentShader(),
            vertexShader: vertexShader(),
          }),
        };
        break;
      case Materials.Wireframe:
        this.material = {
          type: Materials.Wireframe,
          material: new MeshBasicMaterial({
            color: 0xe4e4e4,
            side: DoubleSide,
            wireframe: true,
          }),
        };
        break;
      case Materials.Phong:
        this.material = {
          type: Materials.Phong,
          material: new MeshPhongMaterial({
            color: 0xe4e4e4,
            side: DoubleSide,
          }),
        };
        break;
    }
  }

  getMaterial = () => this.material.material;
}

export default Material;
