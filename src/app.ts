import Viewer from "./modules/terrainviewer";
import Movement from "./modules/movement";
import setup from "./setup";

//* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//* SETUP SCENE, RENDERER AND CAMERA
//* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
const { scene, renderer, camera } = setup();

const movement = new Movement(camera);

const viewer = new Viewer(6, 300);
scene.add(viewer.getViewer());

renderer.renderOnce(camera);

const render = () => {
  requestAnimationFrame(render);

  const { isMoving, deltaX, deltaZ } = movement.calculateMovement();

  if (isMoving) {
    let newX = (viewer.position.x += deltaX);
    let newZ = (viewer.position.z += deltaZ);

    const newPosition = {
      x: newX,
      z: newZ,
    };

    viewer.moveTo(newPosition);
  }

  renderer.renderOnce(camera);
};

render();
