const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors);

app.get("/terrain/:position/:latChunk/:lonChunk", (req, res) => {
  console.log("Terrain request");
  res.header("content-type", "application/json");
  const response = {
    position,
    latChunk,
    lonChunk,
  };

  res.json(response);
  res.end();
});

app.get("/", (req, res) => {
  console.log("Mainpage request");
  res.send("Nothing here");
  res.end();
});

app.listen(8081, () => {
  console.log("Listening on 8081");
});
